const request = require('request');

const api = function(serverDomain) {
	function createResponseHandler(playCB) {
		function handleResponse(error, response, body) {
			if (!error && response.statusCode == 200) {
				if (typeof(body) === "string") {
					body = JSON.parse(body);
				}
				
				if (body.error) {
					console.error(body.error);
				}
				else {
					if (body.gameOver) {
						console.log("Game over. Here are the results :");
						body.results.nbCuredPatients = body.results.curedPatients.length;
						console.log(body.results);
					}
					else {
						play(body.gameId, body.state);
					}
				}
			}
			else {
				if (error) {
					console.log(error);
				}
				else {
					console.log(body);
				}
			}
		}
		
		function play(gameId, curState) {
			Promise.resolve(playCB(curState)).then((move) => {
				if (move != null) {
					var options = {
						uri: serverDomain + '/api/play',
						method: 'POST',
						json: {gameId: gameId, action:move}
					};
					
					request.post(options, handleResponse);
				}
			});
		}
		
		return handleResponse;
	}
	
	return {
		start: (patientId, playCB) => {
			var options = {
				uri: serverDomain + '/api/start',
				method: 'POST',
				json: {patientId: patientId}
			};
			request.post(options, createResponseHandler(playCB));
		},
		evaluate: (teamName, playCB) => {
			var options = {
				uri: serverDomain + '/api/evaluate',
				method: 'POST',
				json: {teamName: teamName}
			};
			request.post(options, createResponseHandler(playCB));
		}
	}
};

module.exports = api;