const serverDomain = 'http://localhost:3000'; //Change this to the domain of the server your want to use
const apiWrapper = require('./api-wrapper')(serverDomain);

//Fill in this function with your code. It will be executed each time it's your turn to move
//This function can return a Promise or a synchronous value.
function turn(curstate) {
	return new Promise((resolve, reject) => {
		//Write your code here. Use resolve(<action>) to return the action you want to do.
		//See example below
		if (curstate.visitCount == 1) {
			resolve({
				type: "WAIT"
			});
		}
		else {
			resolve({
				type: "TREATMENT",
				treatment: "Antiviral1"
			});
		}
	});
}

//To run your code with only one patient, use this function. The integer is the id of the patient
apiWrapper.start(1, turn);

//To test your code and evaluate your score, use this function. Your code will run for all the patients available
//apiWrapper.evaluate(YOUR_TEAM_NAME, turn);
